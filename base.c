#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>
//Esta funcion esta hecha para poder sacar la desviacion estandar de los promedios del curso
float desvStd(estudiante curso[]){
	int i=0, sum1=0, a=0;//Se definen 3 variables enteras para el funcionamiento correcto de la funcion
	float prom1=0, num0, num1, b, c=0;//Se definen 3 variables flotantes para correcto funcionamiento de la funcion
	for (i=0; i<24; i++){//Se crea una estructura repetitiva para sumar los promedios y obtener el total de promedios existentes
		if (curso[i].prom!=0){//Se crea una condicion que si el promedio del curso es distinto de 0, se hace lo de abajo
			prom1=prom1+curso[i].prom;
			a++;
		}
	}
	prom1=prom1/a;//Se divide la suma del promedio con el total, para sacar el promedio general del curso
	for(i=0; i<24; i++){//Se crea una estructura repetitiva para poder sacar el denominador de la Desviacion Estandar
		num0=curso[i].prom-prom1;
		num1=pow(num0,2);
		sum1=sum1+num1;
	}
	//Para finalizar el proceso de la desviacion estandar, se divide todo lo obtenido
	//por la cantidad de promedios, ademas, se debe sacar la raiz cuadrada de lo obtenido anteriormente
	//y para finalizar, retornar el numero correspondiente a la desviacion estandar.
	b=sum1/a;
	c=sqrt(b);
	return c;
}
//Esta funcion esta encargada de sacar el menor promedio del curso
float menor(estudiante curso[]){
	float prome=7.0;//Se define un promedio maximo para despues compararlo y sacar el menor
	int i=0;
	for (i=0; i<24; i++){//Se define una estructura repetitiva para poder comparar los promedios y poder sacar el menor
		if (curso[i].prom<prome){//Si el promedio del curso es menor al promedio definido arriba es menor
			prome=curso[i].prom;//El promedio menor es el comparado anteriormente
		}
	}
	return prome;//Se retorna el promedio
}
//Esta funcion esta encargada de sacar el mayor promedio del curso
float mayor(estudiante curso[]){
	float proma=1.0;//Se define el menor promedio para despues compararlo y poder sacar le mayor
	int i=0;
	for (i=0; i<24; i++){//Se crea una estructura repetitiva para poder comparar los promedios y poder sacar el mayor
		if (curso[i].prom>proma){//Si el promedio del curso es mayor al promedio definido arriba
			proma=curso[i].prom;//El priomedio mayor es el comparado anteriormente
		}
	}
	return proma;//Se retorna el promedio mayor
}
//Esta funcion está encargada de sacar el promedio de cada estudiante.
void obtenerPromedio(estudiante curso[], int i){
	//Se define el promedio del curso curso[i].prom por el ciclo en el pasará para poder asignarle las notas corresspondientes a las variables y asi poder sacar el promedio.
	curso[i].prom=(curso[i].asig_1.proy1*0.20)+(curso[i].asig_1.proy2*0.20)+(curso[i].asig_1.proy3*0.30)+(curso[i].asig_1.cont1*0.05)+(curso[i].asig_1.cont2*0.05)+(curso[i].asig_1.cont3*0.05)+(curso[i].asig_1.cont4*0.05)+(curso[i].asig_1.cont5*0.05)+(curso[i].asig_1.cont6*0.05);
}
//Esta funcion, el unico rol que cumple es pedir las notas al usuario (profesor) y asignarla a las variables correspondientes, ademas de sacar el promedio de las notass con la funcion definida anteriormente
void registroCurso(estudiante curso[]){
	int i;
	printf("HA ELEGIIDO INGRESAR LAS CALIFICACIONES\n");
	for (i=0; i<24; i++){
		printf("Ingrese la nota del proyecto 1 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.proy1);
		printf("Ingrese la nota del proyecto 2 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.proy2);
		printf("Ingrese la nota del proyecto 3 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.proy3);
		printf("Ingrese la nota del control 1 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont1);
		printf("Ingrese la nota del control 2 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont2);
		printf("Ingrese la nota del control 3 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont3);
		printf("Ingrese la nota del control 4 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont4);
		printf("Ingrese la nota del control 5 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont5);
		printf("Ingrese la nota del control 6 para el estudiante %d\n", i);
		scanf("%f", &curso[i].asig_1.cont6);
		obtenerPromedio(curso, i);

	}
}
//Esta funcion está encargada de clasificar los estudiantes en aprobados o reprobados, creando un archivo para cada categoria
void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *clasiEstAP;//Se define una variable archivo para los aprobadoss
	if((clasiEstAP=fopen("Aprobados", "w"))==NULL){//Si al abrir el archivo en modo escritura se retorna un NULL, se imprime el error correspondiente
		printf("Error al crear el archivo\n");
		exit(0);
	}
	else{//Si no es asi, se crea una funcion repetitiva para recorrer el promedio del estudiante y si es mayor que 4, se va printeando en el archivo denominado "Aprobados" y despues se cierra el archivo.
		int i;
		for (i=0; i<24; i++){
			if(curso[i].prom>=4.0){
				fprintf(clasiEstAP, "%s\t%s\t%s\t%.1f\n", curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM, curso[i].prom);
			}
		}
	}
	printf("*************\n");
	printf("El registro de alumnos aprobados fue realizado con exito\n");
	printf("*************\n");
	fclose(clasiEstAP);
	FILE *clasiEstREP;
	if((clasiEstREP=fopen("Reprobados", "w"))==NULL){//Si al abrir el archivo en modo escritura se retorna un NULL, se imprime el error correspondiente
		printf("Error al crear el archivo\n");
		exit(0);
	}
	else{
		int i;
		for (i=0; i<24; i++){//Si no es asi, se crea una funcion repetitiva para recorrer el promedio del estudiante y si es menor que 4, se va printeando en el archivo denominado "Reprobados" y despues se cierra el archivo.
			if (curso[i].prom<4.0){
				fprintf(clasiEstREP, "%s\t%s\t%s\t%.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].prom);
			}
		}
	}
	printf("*************\n");
	printf("El registro de los estudiantes Reprobados fue realizado con exito\n");
	printf("*************\n");
	fclose(clasiEstREP);
}
//Esta funcion está encargada de imprimir el promedio mayor, el promedio menor y la desviacion estandar llamando a lass funciones correspondientes.
void metricasEstudiantes(estudiante curso[]){
	printf("Las metricas de los estudiantes en general, son las siguientes:\n");
	float maj=mayor(curso), minor=menor(curso), stddesv=desvStd(curso);
	printf("Promedio Mayor: %f\n", maj);
	printf("Promediio Menor: %f\n", minor);
	printf("Desviacion Estandar: %f\n", stddesv);
}
//Esta funcion es el menu, el cual es el encargado de invocar a todas las funcioness hechas anteriormente
void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2: registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}
//Funcion Main, encargada de invocar al menu y defiinir estudiante curso.
int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}